from django.forms import ModelForm
from tasks.models import Task


# create a form model for the creation of new tasks
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ("name", "start_date", "due_date", "project", "assignee")
