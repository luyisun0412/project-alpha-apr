from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create a view for displaying the projects belonging to the logged-in user
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)

    context = {"projects": projects}
    return render(request, "projects/list_projects.html", context)


# Create a view to display the details of a particular project
@login_required()
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "projects/show_project.html", context)


# Create a view to allow users to create a new project
@login_required()
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
