from django.forms import ModelForm
from projects.models import Project


# create a form model for the creation of the new project
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ("name", "description", "owner")
